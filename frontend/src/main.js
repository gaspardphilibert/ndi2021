import { createApp } from 'vue'
import router from './router'
import store from './store/store.js'
import App from './App.vue'
import PrimeVue from 'primevue/config';

import 'primeicons/primeicons.css';
import '../node_modules/primeflex/primeflex.css';
import 'primevue/resources/themes/nova-alt/theme.css'
import 'primevue/image';
import 'primevue/inputtext';

const app = createApp(App)
app.use(router)
app.use(PrimeVue)
app.use(store)
app.mount("#app")
