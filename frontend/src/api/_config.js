import axios from 'axios'
import store from '../store/store'

/**
Create a client configured the right way to send http json-formatted request
the base url correspond to the url of the backend server
*/
const host = import.meta.env.PROD ? import.meta.env.VITE_IP : "localhost";
const port = import.meta.env.PROD ? import.meta.env.VITE_PORT : "8000";
    
const client = axios.create({
    baseURL: `http://${host}:${port}`,
    json: true,
});

/**
  Method to use to forge a http request, useful for clean implementation, see example below:
  @tutorial
  getPosts () {
    return this.execute('get', '/posts')
  }
  @param {String} method 'GET', 'POST'
  @param {String} resource the endpoint to use
  @param {Object} [options]
  @param {Object} [options.data] the data to send
*/
export async function execute(method, resource, options) {
    if (options == undefined) {
        options = {
            data: {}
        }
    }

    // inject the Access Token for each request
    if (!options.formData) {
        let accessToken = store.state.auth.token
        options.data.token = accessToken
    }

    return client({
        method,
        url: resource,
        data: options.data,
        headers: options.headers || null
    })
        .then(req => { return req.data })
        .catch(error => { throw error.response })
}
