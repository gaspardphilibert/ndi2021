import store from "../store/store";
import { execute } from "./_config.js";

export async function logIn(username, password) {
    try {
        const data = { username, password };
        let connexion = await execute("post", "/authentication_token", { data });

        if (connexion.error) {
            throw connexion.error
        }
        store.commit("auth/change_token", connexion.token)

    } catch (error) {
        throw error;
    }
}

export function loggedIn() {
    try {
        let token = store.state.auth.token
        if (!token) { return false }
        return true
        // let result = await execute('post', "/odoo/isvalid")
        // if (!result.valid) { store.commit("auth/logout") }
        // return result.valid
    } catch {
        return false
    }
}

