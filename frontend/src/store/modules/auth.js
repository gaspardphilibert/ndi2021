const state = () => ({
    token: '',
})

const mutations = {
    logout(state) {
        state.token = ''
    },
    change_token(state, token) { state.token = token },
}

export default {
    namespaced: true,
    state,
    mutations
}
