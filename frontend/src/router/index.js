import { createRouter, createWebHistory } from "vue-router";
import Acceuil from "../components/Acceuil.vue"
import PageNotFound from "../components/PageNotFound.vue"
import Login from "../components/Login.vue"
import DashBoard from "../components/DashBoard.vue"
import ViewStory from "../components/ViewStory.vue"
//import Stories from "../components/Stories.vue"
import RescueTable from "../components/RescueTable.vue"
import RescueCard from "../components/RescueCard.vue"
import { loggedIn } from "../api/auth.js"

const routes = [
    {
        path: "/",
        name: "Acceuil",
        component: Acceuil,
    },
    {
        path: "/stories",
        name: "Stories",
        component: RescueTable,
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    {
        path: "/dashboard",
        name: "DashBoard",
        component: DashBoard,
        meta: { requiresAuth : true}
    },
    {
        path: "/viewstory",
        name: "ViewStory",
        props: true,
        component: RescueCard,
    },
    {
        path: "/:pathMatch(.*)*",
        name: "404",
        component: PageNotFound,
    },
    
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});


router.beforeEach(async (to, _, next) => {
    if (to.matched.some(record => record.name == "Login")) {
        let isLoggedIn = await loggedIn();
        if (isLoggedIn) { next('/') }
        else { next(); return }
    }
    else if (to.matched.some(record => record.meta.requiresAuth)) {
        let isLoggedIn = await loggedIn();
        if (isLoggedIn) { next() }
        else { next('/login') }
    } else {
        next()
    }

})

export default router;

