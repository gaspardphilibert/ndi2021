<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BoatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BoatRepository::class)
 */
#[ApiResource]
class Boat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=Rescue::class, mappedBy="boat", cascade={"persist", "remove"})
     */
    private $rescue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRescue(): ?Rescue
    {
        return $this->rescue;
    }

    public function setRescue(?Rescue $rescue): self
    {
        // unset the owning side of the relation if necessary
        if ($rescue === null && $this->rescue !== null) {
            $this->rescue->setBoat(null);
        }

        // set the owning side of the relation if necessary
        if ($rescue !== null && $rescue->getBoat() !== $this) {
            $rescue->setBoat($this);
        }

        $this->rescue = $rescue;

        return $this;
    }
}
