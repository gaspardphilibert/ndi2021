<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RescueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RescueRepository::class)
 */
#[ApiResource]
class Rescue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=Boat::class, inversedBy="rescue", cascade={"persist", "remove"})
     */
    private $boat;

    /**
     * @ORM\OneToMany(targetEntity=RescuedPerson::class, mappedBy="rescue")
     */
    private $rescued_persons;

    /**
     * @ORM\ManyToMany(targetEntity=Rescuer::class, inversedBy="rescues")
     */
    private $rescuers;

    public function __construct()
    {
        $this->rescued_persons = new ArrayCollection();
        $this->rescuers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBoat(): ?Boat
    {
        return $this->boat;
    }

    public function setBoat(?Boat $boat): self
    {
        $this->boat = $boat;

        return $this;
    }

    /**
     * @return Collection|RescuedPerson[]
     */
    public function getrescued_persons(): Collection
    {
        return $this->rescued_persons;
    }

    public function addRescuedPerson(RescuedPerson $rescuedPerson): self
    {
        if (!$this->rescued_persons->contains($rescuedPerson)) {
            $this->rescued_persons[] = $rescuedPerson;
            $rescuedPerson->setRescue($this);
        }

        return $this;
    }

    public function removeRescuedPerson(RescuedPerson $rescuedPerson): self
    {
        if ($this->rescued_persons->removeElement($rescuedPerson)) {
            // set the owning side to null (unless already changed)
            if ($rescuedPerson->getRescue() === $this) {
                $rescuedPerson->setRescue(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rescuer[]
     */
    public function getRescuers(): Collection
    {
        return $this->rescuers;
    }

    public function addRescuer(Rescuer $rescuer): self
    {
        if (!$this->rescuers->contains($rescuer)) {
            $this->rescuers[] = $rescuer;
        }

        return $this;
    }

    public function removeRescuer(Rescuer $rescuer): self
    {
        $this->rescuers->removeElement($rescuer);

        return $this;
    }
}
