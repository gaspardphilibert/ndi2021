<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RescuerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RescuerRepository::class)
 */
#[ApiResource]
class Rescuer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birth;

    /**
     * @ORM\ManyToMany(targetEntity=Rescue::class, mappedBy="rescuers")
     */
    private $rescues;

    public function __construct()
    {
        $this->rescues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirth(): ?\DateTimeInterface
    {
        return $this->birth;
    }

    public function setBirth(?\DateTimeInterface $birth): self
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * @return Collection|Rescue[]
     */
    public function getRescues(): Collection
    {
        return $this->rescues;
    }

    public function addRescue(Rescue $rescue): self
    {
        if (!$this->rescues->contains($rescue)) {
            $this->rescues[] = $rescue;
            $rescue->addRescuer($this);
        }

        return $this;
    }

    public function removeRescue(Rescue $rescue): self
    {
        if ($this->rescues->removeElement($rescue)) {
            $rescue->removeRescuer($this);
        }

        return $this;
    }
}
