<?php

namespace App\Repository;

use App\Entity\RescuedPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RescuedPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method RescuedPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method RescuedPerson[]    findAll()
 * @method RescuedPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RescuedPersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RescuedPerson::class);
    }

    // /**
    //  * @return RescuedPerson[] Returns an array of RescuedPerson objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RescuedPerson
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
