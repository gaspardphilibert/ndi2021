<?php

namespace App\Repository;

use App\Entity\Rescuer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rescuer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rescuer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rescuer[]    findAll()
 * @method Rescuer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RescuerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rescuer::class);
    }

    // /**
    //  * @return Rescuer[] Returns an array of Rescuer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rescuer
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
