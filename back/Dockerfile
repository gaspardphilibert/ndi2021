FROM alpine:latest
WORKDIR /app
ARG XDEBUG_MODE
RUN apk add curl unzip wget bash openssl
RUN apk add php8-fpm php8-intl php8-openssl php8-tokenizer php8-pdo_mysql php8-sodium php8-common php8-fileinfo php8-mbstring php8-dev php8-xmlreader php8-opcache php8-posix php8-session php8-pecl-xdebug php8-json php8-xml php8-iconv php8-curl php8-phar php8-pecl-apcu php8-zip php8-ctype php8-pecl-mcrypt php8-bcmath php8 php8-dom php8-pdo php8-simplexml php8-xmlwriter php8-gd
RUN ln -s /usr/bin/php8 /usr/bin/php
RUN curl https://getcomposer.org/installer | php8 && mv composer.phar /usr/bin/composer
RUN echo "zend_extension=xdebug.so" > /etc/php8/conf.d/50_xdebug.ini \
  && echo "xdebug.mode=$XDEBUG_MODE" >> /etc/php8/conf.d/50_xdebug.ini \
  && echo "xdebug.client_host = host.docker.internal" >> /etc/php8/conf.d/50_xdebug.ini
RUN sed -i -e "s/127.0.0.1:9000/0.0.0.0:9000/g" /etc/php8/php-fpm.d/www.conf

# Create a group and user
RUN addgroup -S app -g 1000 && adduser -S app -G app -u 1000
RUN sed -i -e "s/user = nobody/user = app/g" /etc/php8/php-fpm.d/www.conf
RUN sed -i -e "s/group = nobody/group = app/g" /etc/php8/php-fpm.d/www.conf
RUN chown -R app:app .
RUN chown -R app:app /var/log/php8

# Enable error logging \
RUN sed -i -e "s/;catch_workers_output = yes/catch_workers_output = yes/g" /etc/php8/php-fpm.d/www.conf

USER app
CMD php-fpm8 -F
