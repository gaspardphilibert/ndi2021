<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211202231817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rescued_person ADD rescue_id INT NOT NULL');
        $this->addSql('ALTER TABLE rescued_person ADD CONSTRAINT FK_513209A2577D92CC FOREIGN KEY (rescue_id) REFERENCES rescue (id)');
        $this->addSql('CREATE INDEX IDX_513209A2577D92CC ON rescued_person (rescue_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rescued_person DROP FOREIGN KEY FK_513209A2577D92CC');
        $this->addSql('DROP INDEX IDX_513209A2577D92CC ON rescued_person');
        $this->addSql('ALTER TABLE rescued_person DROP rescue_id');
    }
}
