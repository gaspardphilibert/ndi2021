<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211202230533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE boat (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rescue (id INT AUTO_INCREMENT NOT NULL, boat_id INT DEFAULT NULL, date DATE NOT NULL, description LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_FA71973BA1E84A29 (boat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rescue_rescuer (rescue_id INT NOT NULL, rescuer_id INT NOT NULL, INDEX IDX_99821752577D92CC (rescue_id), INDEX IDX_99821752CD482949 (rescuer_id), PRIMARY KEY(rescue_id, rescuer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rescued_person (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rescuer (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, birth DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rescue ADD CONSTRAINT FK_FA71973BA1E84A29 FOREIGN KEY (boat_id) REFERENCES boat (id)');
        $this->addSql('ALTER TABLE rescue_rescuer ADD CONSTRAINT FK_99821752577D92CC FOREIGN KEY (rescue_id) REFERENCES rescue (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rescue_rescuer ADD CONSTRAINT FK_99821752CD482949 FOREIGN KEY (rescuer_id) REFERENCES rescuer (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rescue DROP FOREIGN KEY FK_FA71973BA1E84A29');
        $this->addSql('ALTER TABLE rescue_rescuer DROP FOREIGN KEY FK_99821752577D92CC');
        $this->addSql('ALTER TABLE rescue_rescuer DROP FOREIGN KEY FK_99821752CD482949');
        $this->addSql('DROP TABLE boat');
        $this->addSql('DROP TABLE rescue');
        $this->addSql('DROP TABLE rescue_rescuer');
        $this->addSql('DROP TABLE rescued_person');
        $this->addSql('DROP TABLE rescuer');
    }
}
