.PHONY: exec

up:
	docker-compose up -d
stop:
	docker-compose stop
exec:
	docker-compose exec -w /app/back fpm bash
composer_install:
	docker-compose exec -w /app/back fpm composer install
jwt_token:
	docker-compose exec -w /app/back fpm php bin/console lexik:jwt:generate-keypair

install: up composer_install jwt_token
